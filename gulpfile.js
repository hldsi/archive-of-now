"use strict";

const gulp = require("gulp");
const twing = require("gulp-twing");
const sass = require('gulp-sass');
const terser = require('gulp-terser');
const merge = require('merge-stream');
const del = require('del');

sass.compiler = require('node-sass');

// setup for Twig compilation
const { TwingEnvironment, TwingLoaderRelativeFilesystem } = require("twing");
const loader = new TwingLoaderRelativeFilesystem();

function clean() {
  return del([
    'public/**/*',
  ]);
}

function twig() {
  // This is the fix for watching:
  // performance hit is negligible
  const env = new TwingEnvironment(loader);

  return gulp.src("src/**/*.twig")
    .pipe(twing(env))
    .pipe(gulp.dest("public/"));
}

function css() {
  return gulp.src('src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/css/'));
}

function js() {
  const paths = [
    { src: 'src/js/**/*.js', dest: 'public/js/' },
    { src: 'node_modules/jquery/dist/*.min.js', dest: 'public/js/' },
    { src: 'node_modules/jspdf/dist/*.min.js', dest: 'public/js/' }
  ];

  const tasks = paths.map(function (path) {
    return gulp.src(path.src).pipe(terser()).pipe(gulp.dest(path.dest));
  });

  return merge(tasks);
}

function images() {
  return gulp.src('src/images/**/*')
    .pipe(gulp.dest('public/images/'));
}

function konva() {
  return gulp.src('node_modules/konva/*.min.js')
    .pipe(gulp.dest('public/js/'));
}

function bootstrap() {
  const paths = [
    { src: 'node_modules/bootstrap/dist/js/*.min.js', dest: 'public/js/' },
    { src: 'node_modules/bootstrap/dist/js/*.min.js.map', dest: 'public/js/' },
    { src: 'node_modules/bootstrap/dist/css/*.min.css', dest: 'public/css/' },
    { src: 'node_modules/bootstrap/dist/css/*.min.css.map', dest: 'public/css/' }
  ];

  const tasks = paths.map(function (path) {
    return gulp.src(path.src).pipe(gulp.dest(path.dest));
  });

  return merge(tasks);
}

function slick() {
  const paths = [
    { src: 'node_modules/slick-carousel/slick/*.min.js', dest: 'public/js/' },
    { src: 'node_modules/slick-carousel/slick/*.css', dest: 'public/css/' }
  ];

  const tasks = paths.map(function (path) {
    return gulp.src(path.src).pipe(gulp.dest(path.dest));
  });

  return merge(tasks);
}

// Build task compile twig, sass, js and images.
gulp.task('build', gulp.series(clean, css, js, images, bootstrap, konva, twig, slick));

module.exports = {
  twig,
  css,
  js,
  images,
  bootstrap,
  konva,
  clean,
  slick,
};