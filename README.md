# Archive of Now


## Overview 

The Archive of Now is a collaboration between Harvard Libraries and metaLAB that proposes to make Harvard Library digital collections readily available for use and remix, addressing conceptual questions about archives, digital media, and the use of remix and appropriation in digital culture.

## Requirements

1. [NodeJS](https://nodejs.org/en/download/)
2. GulpJS [Globally](https://docs.npmjs.com/getting-started/installing-npm-packages-globally)
   - Run `sudo npm install -g gulp-cli` from your command line

## Installation
To get started with this project, use the following commands:

```
# Clone the repo to your git
> git clone https://gitlab.com/hldsi/archive-of-now.git

# Enter the folder that was created by the clone
> cd archive-of-now

# Install node dependencies
> npm install

# Build the public pages
> gulp build
```

## GitLab Pages for this project
This project is viewable at [https://hldsi.gitlab.io/archive-of-now/](https://hldsi.gitlab.io/archive-of-now/)

