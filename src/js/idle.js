(function () {
  let minutes = true; // change to false if you'd rather use seconds
  let interval = minutes ? 60000 : 1000;
  let IDLE_MODAL_TIMEOUT = 3;
  let IDLE_RESET_TIMEOUT = 5;
  let idleCounter = 0;
  let filename = window.location.href.split('/').pop().split('#')[0].split('?')[0];

  document.onmousemove = document.onkeypress = function () {
    idleCounter = 0;
  };

  window.setInterval(function () {
    if (filename !== 'index.html') {
      if (++idleCounter >= IDLE_MODAL_TIMEOUT) {
        $('#idle-modal').modal('show');
      }
      if (++idleCounter >= IDLE_RESET_TIMEOUT) {
        sessionStorage.clear();
        window.location.href = "index.html";
      }
    }
  }, interval);
}());