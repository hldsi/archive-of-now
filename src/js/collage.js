let width = $("#collage-container").width();
let height = window.innerHeight;
let imageGroup = [];
let shapeGroup = [];
let transformers = [];
let selectedShapes = [];
let ratios = [];
let anchorTypes = ['.topLeft', '.topRight', '.bottomLeft', '.bottomRight', '.deleteButton'];
// Variables for mouse rectangle work.
let posStart, posNow, mode = '';
let selectedItems = [];
let savedCollage = {};
let savedImageFilters = {};
const promises = [];

selectedItems = JSON.parse(sessionStorage.getItem('selectedItems'));
if (selectedItems == null){
  selectedItems = [];
}

// A $( document ).ready() block.
$( document ).ready(function() {
  resetTools();
  addShapesToolbox();
});

function resetTools() {
  // Initialize the tools.
  $('#crop-toggle').prop('checked', false);
  $('#blur-slider').val(0);
  $('#opacity-slider').val(1);
  $('#blend-none').prop("checked",true);

}

function opacityFilter(imageData) {

}

function getRelativePointerPosition(node) {
  // the function will return pointer position relative to the passed node
  var transform = node.getAbsoluteTransform().copy();
  // to detect relative position we need to invert transform
  transform.invert();

  // get pointer (say mouse or touch) position
  var pos = node.getStage().getPointerPosition();

  // now we find relative point
  return transform.point(pos);
}

function getOriginalDimensions(itemID) {
  //create our deferred object
  var def = $.Deferred();

  $.getJSON('https://iiif.lib.harvard.edu/manifests/ids:'+itemID)
    .done(function(data){
      //resolve the deferred, passing it our custom data
      def.resolve({
        width:data['sequences'][0]['canvases'][0]['images'][0]['resource']['width'],
        height:data['sequences'][0]['canvases'][0]['images'][0]['resource']['height']
      });
    });
  //return the deferred for listening
  return def;
}

function addCropper(group, x, y, name) {
  let stage = group.getStage();
  let layer = group.getLayer();
  let cropper = new Konva.Rect({x: x, y: y, width: 0, height: 0, stroke: 'red', dash: [2,2], name: name});
  cropper.listening(false); // stop cropper catching our mouse events otherwise if we reverse mouse direction events may not fire
  group.add(cropper);
  cropper.hide();

  group.on('transform', () => {
    cropper.x(group.getWidth());
  });

// start the rubber rect drawing on mouse down.
  group.on('mousedown', function(e){
    if($('#crop-toggle').is(":checked")) {
      group.draggable(false);
      mode = 'drawing';
      let pos = getRelativePointerPosition(group);
      let groupX = group.getX();
      let groupY = group.getY();
      startDrag({x: pos.x, y: pos.y});
    }

  });

// update the rubber rect on mouse move - note use of 'mode' var to avoid drawing after mouse released.
  group.on('mousemove', function(e){
    if($('#crop-toggle').is(":checked")) {
      let pos = getRelativePointerPosition(group);
      if (mode === 'drawing'){
        updateDrag({x: pos.x, y: pos.y}, cropper);
      }
    }
  });

// When user releases the mouse we note the size and modify the clip rect.
  group.on('mouseup', function(e){
    if($('#crop-toggle').is(":checked")) {
      mode = '';
      cropper.visible(false);
      let x = cropper.getX();
      let y = cropper.getY();
      let width = cropper.width();
      let height = cropper.height();
      let groupID = group.id();
      group.draggable(true);
      layer.draw();

      image = group.findOne('Image');
      if (image.getClassName() === 'Image') {
        let naturalHeight = image.attrs.image.naturalHeight;
        let naturalWidth = image.attrs.image.naturalWidth;
        let currentWidth = image.width();
        let currentHeight = image.height();
        let resizeWidth = currentWidth / naturalWidth;
        let resizeHeight = currentHeight / naturalHeight;
        let ratio = Math.min(resizeWidth, resizeHeight);
        image.clearCache();
        image.crop({
          x: x / ratio,
          y: y / ratio,
          width: width / ratio,
          height: height / ratio
        });

        layer.batchDraw();
        image.cache();
      }

      transformers[groupID].forceUpdate();
      layer.draw();
    }
  });

}

let stage = new Konva.Stage({
  container: 'collage-container',
  width: width,
  height: height
});

let layer = new Konva.Layer();
stage.add(layer);

// what is url of dragging element?
let itemURL = '';
document
  .getElementById('picker-row')
  .addEventListener('dragstart', function(e) {
    itemURL = e.target.src;
    itemURL = itemURL.replace('/,150/', '/full/');
  });

let con = stage.container();
con.addEventListener('dragover', function(e) {
  e.preventDefault(); // !important
});

con.addEventListener('drop', function(e) {
  e.preventDefault();

  // Turn off cropping when adding a new image to the canvas.
  let crop_enabled = $('#crop-toggle').is(':checked');
  if (crop_enabled) {
    $( "#crop-toggle" ).click();
  }

  // now we need to find pointer position
  // we can't use stage.getPointerPosition() here, because that event
  // is not registered by Konva.Stage
  // we can register it manually:
  stage.setPointersPositions(e);


  Konva.Image.fromURL(itemURL, function(image) {
    let children = layer.getChildren();
    let index = children.length;
    let itemID = itemURL.match(/(\d+)/);
    let imageID = 'image-'+itemID[0]+'-'+index;
    let groupID = 'group-'+itemID[0]+'-'+index;
    resetTools();

    getOriginalDimensions(itemID[0])
      .done(function(returnData){
        let originalWidth = returnData['width'];
        let originalHeight = returnData['height'];
        let resizeWidth = 500 / originalWidth;
        let resizeHeight = 500 / originalHeight;
        let ratio = Math.min(resizeWidth, resizeHeight);
        ratios[imageID] = ratio;
        let newWidth = originalWidth * ratio;
        let newHeight = originalHeight * ratio;
        newWidth = parseFloat(newWidth.toFixed());
        newHeight = parseFloat(newHeight.toFixed());
        image.id(imageID);
        image.width(newWidth);
        image.height(newHeight);
        image.blurRadius(0);
        image.opacity(1);
        image.attrs.globalCompositeOperation = "none";

        imageGroup[groupID] = new Konva.Group({
          x: newWidth,
          y: newHeight,
          draggable: true,
          position: stage.getPointerPosition(),
          id: (groupID)
        });
        image.cache();
        image.filters([Konva.Filters.Blur, opacityFilter]);
        layer.add(imageGroup[groupID]);
        imageGroup[groupID].add(image);
        // create new transformer
        transformers[groupID] = new Konva.Transformer({
          centeredScaling: true,
          rotationSnaps: [0, 90, 180, 270],
          resizeEnabled: true
        });
        layer.add(transformers[groupID]);
        transformers[groupID].attachTo(imageGroup[groupID]);
        addCropper(imageGroup[groupID], 0, 0, 'cropperRectangle');
        layer.draw();
        imageGroup[groupID].moveToTop();

        imageGroup[groupID].on('click', function() {
          this.moveToTop();
          layer.draw();
          this.getChildren(function(node){
            if (node.getClassName() === 'Image') {
              $('#blur-slider').val(node.blurRadius());
              $('#opacity-slider').val(node.opacity());
              var blendMode = node.attrs.globalCompositeOperation;
              $('#blend-'+blendMode).prop("checked",true);
              return false;
            }
          });
        });

        imageGroup[groupID].on('dragover', function() {
          this.moveToTop();
          layer.draw();
        });
      });
  });
});

selectedItems = getSelectedItems();
selectedItems.forEach(function(value, index) {
  promises.push(
    $.getJSON(value,
      function(data, err) {
        if (err !== 'success') {
          console.log('Something went wrong: ' + err);
        } else {
          let thumbnail = data['sequences'][0]['canvases'][0]['thumbnail']['@id'];
          let thumbnail_array = thumbnail.split("/");
          let image_id = thumbnail_array[5];
          $("<div></div>")
            .addClass("col-sm-12")
            .addClass("col-md-12")
            .addClass("col-lg-6")
            .addClass("picker-image")
            .attr('id', 'picker-image-'+index)
            .appendTo("#picker-row");
          $("<img>")
            .attr('src', thumbnail)
            .attr('width', 100)
            .attr('draggable', true)
            .attr('id', 'image-'+image_id)
            .appendTo('#picker-image-'+index);
        }
      }));
});

Promise.all(promises).then(function() {
  savedCollage = getCollage();
  if (!$.isEmptyObject(savedCollage)) {
    stage.destroyChildren();
    layer = Konva.Node.create(savedCollage);
    stage.add(layer);
    layer.getChildren(function(node){
      let childImage = node.findOne('Image');
      if (childImage) {
        childImage.crossOrigin = "anonymous"; // This enables CORS
        let childID = childImage.id();
        let groupID = childID.replace("image-", "group-");
        let imageID = childID.substr(0, childID.lastIndexOf('-'));
        let imageObj = new Image();
        imageObj.src = $('#' + imageID).attr('src');
        imageObj.crossOrigin = "Anonymous";
        imageObj.onload = function () {
          stage.find('#' + childID)[0].image(imageObj);
          stage.draw();
        };
      }
      layer.draw();
    });
  }
});

$('#crop-toggle').click(function () {
  if($(this).is(":checked")){
    // select all groups inside layer
    let groups = layer.find('Group');
    for (const group of groups) {
      if (group.id()) {
        group.draggable(false);
      }
      layer.draw();
    }

    for (const anchorType of anchorTypes) {
      let foundAnchors = layer.find(anchorType);
      for (const anchor of foundAnchors) {
        anchor.hide();
        layer.draw();
      }
    }
    $('#collage-container').removeClass('move').addClass('cross-hair');
  }
  else {
    for (const anchorType of anchorTypes) {
      let foundAnchors = layer.find(anchorType);
      for (const anchor of foundAnchors) {
        anchor.show();
        layer.draw();
      }
    }
    // select all groups inside layer
    let groups = layer.find('Group');
    for (const group of groups) {
      if (group.id()) {
        group.draggable(true);
      }
      layer.draw();
    }
    $('#collage-container').removeClass('cross-hair').addClass('move');
  }
});

$('#blur-slider').on('input', function() {
  let topIndex = -1;
  let topGroup = '';
  let sliderValue = this.value;
  layer.getChildren(function(node){
    let currentIndex = node.getAbsoluteZIndex();
    if (currentIndex > topIndex) {
      topIndex = currentIndex;
      topGroup = node.id();
    }
  });
  layer.getChildren(function(node){
    if (node.id() === topGroup) {
      subnode = node.findOne('Image');
      if(!subnode) subnode = node.findOne('Shape');
      subnode.blurRadius(sliderValue);
      let groupID = node.id();
      let blur = {'blurRadius': sliderValue};
      savedImageFilters[groupID] = blur;
      saveImageFilters(savedImageFilters);
      layer.batchDraw();
      return false;
    }
  });
});

$('#opacity-slider').on('input', function() {
  let topIndex = -1;
  let topGroup = '';
  let sliderValue = this.value;
  layer.getChildren(function(node){
    let currentIndex = node.getAbsoluteZIndex();
    if (currentIndex > topIndex) {
      topIndex = currentIndex;
      topGroup = node.id();
    }
  });
  layer.getChildren(function(node){
    if (node.id() === topGroup) {
      subnode = node.findOne('Image');
      if(!subnode) subnode = node.findOne('Shape');
      subnode.opacity(sliderValue);
      layer.batchDraw();
      return false;
    }
  });
});

$("#blend input").change(function(){
  var mode = $(this).val();
  let topIndex = -1;
  let topGroup = '';
  layer.getChildren(function(node){
    let currentIndex = node.getAbsoluteZIndex();
    if (currentIndex > topIndex) {
      topIndex = currentIndex;
      topGroup = node.id();
    }
  });
  layer.getChildren(function(node){
    if (node.id() === topGroup) {
      node.getChildren(function(subnode){
        //if (subnode.getClassName() === 'Image') {
        subnode.attrs.globalCompositeOperation = mode;
        layer.batchDraw();
        return subnode;
        //}
      });
      return false;
    }
  });
});


$('#next-button').click(function () {
  saveCollage();
});

$('#delete-button').on('click', function() {
  let topIndex = -1;
  let topGroup = '';
  layer.getChildren(function(node){
    let currentIndex = node.getAbsoluteZIndex();
    if (currentIndex > topIndex) {
      topIndex = currentIndex;
      topGroup = node.id();
    }
  });
  layer.getChildren(function(node){
    if (node.id() === topGroup) {
      if (topGroup) {
        let groupID = node.id();
        if (imageGroup[groupID]) {
          imageGroup[groupID].destroy();
          transformers[groupID].destroy();
          layer.batchDraw();
        }
        if (shapeGroup[groupID]) {
          shapeGroup[groupID].destroy();
          transformers[groupID].destroy();
          layer.draw();
        }
      }
      return false;
    }
  });
});

// Mouse movement crop functions
function startDrag(posIn){
  posStart = {x: posIn.x, y: posIn.y};
  posNow = {x: posIn.x, y: posIn.y};
}

// update rubber rect position
function updateDrag(posIn, rectangle){
  posNow = {x: posIn.x, y: posIn.y};
  var posRect = reverse(posStart,posNow);
  rectangle.x(posRect.x1);
  rectangle.y(posRect.y1);
  rectangle.width(posRect.x2 - posRect.x1);
  rectangle.height(posRect.y2 - posRect.y1);
  rectangle.visible(true);
  rectangle.zIndex(1000);
  stage.draw(); // redraw any changes.
}

// This is just to reverse co-ords if user drags left / up
function reverse(r1, r2){
  var r1x = r1.x, r1y = r1.y, r2x = r2.x,  r2y = r2.y, d;
  if (r1x > r2x ){
    d = Math.abs(r1x - r2x);
    r1x = r2x; r2x = r1x + d;
  }
  if (r1y > r2y ){
    d = Math.abs(r1y - r2y);
    r1y = r2y; r2y = r1y + d;
  }
  return ({x1: r1x, y1: r1y, x2: r2x, y2: r2y}); // return the corrected rect.
}

function getSelectedItems() {
  selectedItems = JSON.parse(sessionStorage.getItem('selectedItems'));
  if (selectedItems == null){
    selectedItems = [];
  }
  return selectedItems;
}

function saveCollage() {
  sessionStorage.setItem('savedCollage', JSON.stringify(layer));
}

function saveImageFilters(imageFilters) {
  sessionStorage.setItem('savedImageFilters', JSON.stringify(imageFilters));
}

function getCollage() {
  savedCollage = JSON.parse(sessionStorage.getItem('savedCollage'));
  if (savedCollage == null){
    savedCollage = [];
  }
  return savedCollage;
}

function getImageFilters() {
  savedImageFilters = JSON.parse(sessionStorage.getItem('savedImageFilters'));
  if (savedImageFilters == null){
    savedImageFilters = [];
  }
  return savedImageFilters;
}

function addShapesToolbox() {
  let width = 200;
  let height = 200;

  let shapesStage = new Konva.Stage({
    container: 'shapes-container',
    width: width,
    height: height
  });

  let shapesLayer = new Konva.Layer();

  let redCircle = new Konva.Circle({
    x: 10,
    y: 10,
    radius: 10,
    fill: 'red',
  });

  let blackCircle = new Konva.Circle({
    x: 10,
    y: 40,
    radius: 10,
    fill: 'black',
  });

  let blueCircle = new Konva.Circle({
    x: 10,
    y: 70,
    radius: 10,
    fill: 'blue',
  });

  let whiteCircle = new Konva.Circle({
    x: 10,
    y: 100,
    radius: 10,
    fill: 'white',
    stroke: 'black',
    strokeWidth: 1
  });

  let redRectangle = new Konva.Rect({
    x: 30,
    y: 0,
    width: 20,
    height: 20,
    fill: 'red',
  });

  let blackRectangle = new Konva.Rect({
    x: 30,
    y: 30,
    width: 20,
    height: 20,
    fill: 'black',
  });

  let blueRectangle = new Konva.Rect({
    x: 30,
    y: 60,
    width: 20,
    height: 20,
    fill: 'blue',
  });

  let whiteRectangle = new Konva.Rect({
    x: 30,
    y: 90,
    width: 20,
    height: 20,
    fill: 'white',
    stroke: 'black',
    strokeWidth: 1
  });

  let redTriangle = new Konva.RegularPolygon({
    x: 70,
    y: 13,
    sides: 3,
    radius: 13,
    fill: 'red',
  });

  let blackTriangle = new Konva.RegularPolygon({
    x: 70,
    y: 43,
    sides: 3,
    radius: 13,
    fill: 'black',
  });

  let blueTriangle = new Konva.RegularPolygon({
    x: 70,
    y: 73,
    sides: 3,
    radius: 13,
    fill: 'blue',
  });

  let whiteTriangle = new Konva.RegularPolygon({
    x: 70,
    y: 103,
    sides: 3,
    radius: 13,
    fill: 'white',
    stroke: 'black',
    strokeWidth: 1,
  });

  // add the shapes to the layer
  shapesLayer.add(redCircle);
  shapesLayer.add(redRectangle);
  shapesLayer.add(redTriangle);
  shapesLayer.add(blackCircle);
  shapesLayer.add(blackRectangle);
  shapesLayer.add(blackTriangle);
  shapesLayer.add(blueCircle);
  shapesLayer.add(blueRectangle);
  shapesLayer.add(blueTriangle);
  shapesLayer.add(whiteCircle);
  shapesLayer.add(whiteRectangle);
  shapesLayer.add(whiteTriangle);

  // add the layer to the stage
  shapesStage.add(shapesLayer);
  let numberOfShapes = 0;

  redCircle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-' + numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, redCircle, 'circle');
  });

  blackCircle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, blackCircle, 'circle');
  });

  blueCircle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, blueCircle, 'circle');
  });

  whiteCircle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, whiteCircle, 'circle');
  });

  redRectangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, redRectangle, 'rectangle');
  });

  blackRectangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, blackRectangle, 'rectangle');
  });

  blueRectangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, blueRectangle, 'rectangle');
  });

  whiteRectangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, whiteRectangle, 'rectangle');
  });

  redTriangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, redTriangle, 'triangle');
  });

  blackTriangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, blackTriangle, 'triangle');
  });

  blueTriangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, blueTriangle, 'triangle');
  });

  whiteTriangle.on('click', function() {
    numberOfShapes++;
    let groupID = 'shape-'+numberOfShapes;
    shapeGroup[groupID] = new Konva.Group({
      x: 100,
      y: 100,
      draggable: true,
      id: (groupID)
    });
    addShapeToCollage(groupID, whiteTriangle, 'triangle');
  });

}

function addShapeToCollage(groupID, shape, shapeType) {
  
  resetTools();

  let largeShape = shape.clone();
  // clone a shape and override configuration
  largeShape.strokeWidth(0);
  switch(shapeType) {
    case 'circle':
      largeShape.radius(50);
      break;
    case 'rectangle':
      largeShape.width(100);
      largeShape.height(100);
      break;
    case 'triangle':
      largeShape.radius(75);
      break;
  }

  layer.add(shapeGroup[groupID]);
  shapeGroup[groupID].add(largeShape);

  largeShape.off('click');
  shapeGroup[groupID].on('click', function() {
    this.moveToTop();
    this.getChildren(function(node){
      //if (node.getClassName() === 'Shape') {
        $('#blur-slider').val(node.blurRadius());
        $('#opacity-slider').val(node.opacity());
        var blendMode = node.attrs.globalCompositeOperation;
        if(!blendMode) blendMode = "none";
        $('#blend-'+blendMode).prop("checked",true);
        return false;
      //}
    });
    layer.draw();

  });

  // create new transformer
  transformers[groupID] = new Konva.Transformer({
    centeredScaling: true,
    rotationSnaps: [0, 90, 180, 270],
    resizeEnabled: true
  });

  layer.add(transformers[groupID]);
  transformers[groupID].attachTo(shapeGroup[groupID]);
  layer.draw();
  shapeGroup[groupID].moveToTop();

}