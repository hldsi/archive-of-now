const promises = [];
let selectedItems = [];
let categoryList = [
  {
    'name': 'plants',
    'label': 'Plants',
    'images': [
      'https://iiif.lib.harvard.edu/manifests/drs:457731773',
      'https://iiif.lib.harvard.edu/manifests/drs:457731886',
      'https://iiif.lib.harvard.edu/manifests/drs:457730278',
      'https://iiif.lib.harvard.edu/manifests/drs:457604911',
      'https://iiif.lib.harvard.edu/manifests/drs:459469960',
      'https://iiif.lib.harvard.edu/manifests/drs:457730321',
      'https://iiif.lib.harvard.edu/manifests/drs:459469960',
      'https://iiif.lib.harvard.edu/manifests/drs:457730289',
      'https://iiif.lib.harvard.edu/manifests/drs:460199112',
      'https://iiif.lib.harvard.edu/manifests/drs:460198522',
      'https://iiif.lib.harvard.edu/manifests/drs:459301715',
      'https://iiif.lib.harvard.edu/manifests/drs:460199286',
      'https://iiif.lib.harvard.edu/manifests/drs:460199514',
      'https://iiif.lib.harvard.edu/manifests/drs:460198110',
      'https://iiif.lib.harvard.edu/manifests/drs:460198597',
    ]
  },
  {
    'name': 'animals',
    'label': 'Animals',
    'images': [
      'https://iiif.lib.harvard.edu/manifests/ids:1441065',
      'https://iiif.lib.harvard.edu/manifests/ids:1441081',
      'https://iiif.lib.harvard.edu/manifests/ids:1441107',
      'https://iiif.lib.harvard.edu/manifests/ids:1593288',
      'https://iiif.lib.harvard.edu/manifests/ids:1593302',
      'https://iiif.lib.harvard.edu/manifests/ids:1593332',
      'https://iiif.lib.harvard.edu/manifests/ids:1593322',
      'https://iiif.lib.harvard.edu/manifests/ids:1593330',
      'https://iiif.lib.harvard.edu/manifests/ids:1593324',
      'https://iiif.lib.harvard.edu/manifests/ids:1593358',
      'https://iiif.lib.harvard.edu/manifests/ids:43318405',
      'https://iiif.lib.harvard.edu/manifests/drs:6517512',
     ]
  },
  {
    'name': 'figures',
    'label': 'Figures',
    'images': [
      'https://iiif.lib.harvard.edu/manifests/ids:2178624',
      'https://iiif.lib.harvard.edu/manifests/ids:2178608',
      'https://iiif.lib.harvard.edu/manifests/ids:2178612',
      'https://iiif.lib.harvard.edu/manifests/ids:2178636',
      'https://iiif.lib.harvard.edu/manifests/ids:2178626',
      'https://iiif.lib.harvard.edu/manifests/ids:2178606',
      'https://iiif.lib.harvard.edu/manifests/drs:6517520',
      'https://iiif.lib.harvard.edu/manifests/drs:11676309',
      'https://iiif.lib.harvard.edu/manifests/drs:11676301',
      'https://iiif.lib.harvard.edu/manifests/drs:11676308',
      'https://iiif.lib.harvard.edu/manifests/ids:43316781',
      'https://iiif.lib.harvard.edu/manifests/ids:43316793',
      'https://iiif.lib.harvard.edu/manifests/ids:43313849',
      'https://iiif.lib.harvard.edu/manifests/ids:43318030',
      'https://iiif.lib.harvard.edu/manifests/ids:1370195',
      'https://iiif.lib.harvard.edu/manifests/ids:43318393',
      'https://iiif.lib.harvard.edu/manifests/ids:1245079',
      'https://iiif.lib.harvard.edu/manifests/ids:3463912',
      'https://iiif.lib.harvard.edu/manifests/ids:7434533',
    ]
  },
  {
    'name': 'objects-text',
    'label': 'Objects & Text',
    'images': [
      'https://iiif.lib.harvard.edu/manifests/ids:7634412',
      'https://iiif.lib.harvard.edu/manifests/ids:7590957',
      'https://iiif.lib.harvard.edu/manifests/ids:7590969',
      'https://iiif.lib.harvard.edu/manifests/ids:7634435',
      'https://iiif.lib.harvard.edu/manifests/ids:7634442',
      'https://iiif.lib.harvard.edu/manifests/ids:7590978',
      'https://iiif.lib.harvard.edu/manifests/ids:7634448',
      'https://iiif.lib.harvard.edu/manifests/ids:7526339',
      'https://iiif.lib.harvard.edu/manifests/ids:7512310',
      'https://iiif.lib.harvard.edu/manifests/ids:13190698',
      'https://iiif.lib.harvard.edu/manifests/ids:12744142',
      'https://iiif.lib.harvard.edu/manifests/ids:12744141',
      'https://iiif.lib.harvard.edu/manifests/ids:20575336',
      'https://iiif.lib.harvard.edu/manifests/ids:5005742',
    ]
  },
  {
    'name': 'maps-landscapes',
    'label': 'Maps & Landscapes',
    'images': [
      'https://iiif.lib.harvard.edu/manifests/ids:45183774',
      'https://iiif.lib.harvard.edu/manifests/ids:2640168',
      'https://iiif.lib.harvard.edu/manifests/ids:18833853',
      'https://iiif.lib.harvard.edu/manifests/ids:5271075',
      'https://iiif.lib.harvard.edu/manifests/ids:5271081',
      'https://iiif.lib.harvard.edu/manifests/ids:2655327',
      'https://iiif.lib.harvard.edu/manifests/ids:7066372',
      'https://iiif.lib.harvard.edu/manifests/ids:7271044',
      'https://iiif.lib.harvard.edu/manifests/ids:8982541',
      'https://iiif.lib.harvard.edu/manifests/ids:10134969',
      'https://iiif.lib.harvard.edu/manifests/ids:12743770',
      'https://iiif.lib.harvard.edu/manifests/ids:12748456',
      'https://iiif.lib.harvard.edu/manifests/ids:12748414',
      'https://iiif.lib.harvard.edu/manifests/drs:2580900',
      'https://iiif.lib.harvard.edu/manifests/drs:460199483',
    ]
  },
];

categoryList.forEach(function(value, index) {
  $('<li></li>')
    .addClass('nav-item')
    .attr('id', value['name']+'-tab')
    .appendTo('#category-tabs');
  $('<a></a>')
    .addClass('nav-link')
    .attr('href', '#'+value['name'])
    .attr('data-toggle', 'tab')
    .text(value['label'])
    .appendTo('#'+value['name']+'-tab');
  $('<div></div>')
    .attr('id', value['name'])
    .addClass('container')
    .addClass('tab-pane')
    .addClass('fade')
    .appendTo('#category-content');
  $('<div></div>')
    .attr('id', value['name']+'-container')
    .addClass('container')
    .appendTo('#'+value['name']);
  $('<div></div>')
    .attr('id', value['name']+'-row')
    .addClass('row')
    .appendTo('#'+value['name']+'-container');
  if (index === 0) {
    $('#'+value['name']+'-tab').find('a:first').addClass('active');
    $('#'+value['name']).removeClass('fade').addClass('active');
  }

  value['images'].forEach(function(image, imageIndex) {
    promises.push(
    $.getJSON(image,
      function(data, err) {
        if (err !== 'success') {
          console.log('Something went wrong: ' + err);
        } else {
          $("<div></div>")
            .addClass('col-sm-4')
            .addClass('col-md-6')
            .addClass('col-lg-3')
            .addClass('p-5')
            .addClass(value['name'])
            .attr('id', value['name']+'-image-'+imageIndex)
            .attr('data-iiif-manifest', image)
            .data('category', value['name'])
            .data('iiif-manifest', image)
            .appendTo('#'+value['name']+'-row');
          $("<img>")
            .attr('src', data['sequences'][0]['canvases'][0]['thumbnail']['@id'])
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'bottom')
            .attr('title', data['label'])
            .attr('width', 150)
            .attr('draggable', true)
            .addClass('image')
            .appendTo('#'+value['name']+'-image-'+imageIndex);
        }
      }));
  });
});

Promise.all(promises).then(function() {
  selectedItems = getSelectedItems();
  let $selectedItem = '';
  selectedItems.forEach(function(value, index) {
    $selectedItem = $('div').find(`[data-iiif-manifest='${value}']`);
    $selectedItem.removeClass('col-md-6')
      .removeClass('col-lg-3')
      .removeClass('p-5')
      .addClass('p-2');
    $('#current-selection-row').append($selectedItem);
  });
  $('.image').bind('click', function () {
      let $value = $(this);
      let $parent = $value.parent();
      if ($(this).parents('#image-library-container').length === 1) {
        selectedItems = getSelectedItems();
        let numOfItems = selectedItems.length;
        if (numOfItems < 8) {
          $parent.removeClass('col-md-6')
            .removeClass('col-lg-3')
            .removeClass('p-5')
            .addClass('p-2');
          $('#current-selection-row').append($parent);
          selectedItems.push($parent.data('iiif-manifest'));
          saveSelectedItems(selectedItems);
        }
        else {
          $('#item-maximum-alert').show();
        }
      }
      else {
        $parent.addClass('col-md-6')
          .addClass('col-lg-3')
          .removeClass('p-2')
          .addClass('p-5');
        $('#' + $parent.data('category') + '-row').append($parent);
        $('#item-maximum-alert').hide();
        selectedItems = getSelectedItems();
        selectedItems.splice($.inArray($parent.data('iiif-manifest'), selectedItems), 1);
        saveSelectedItems(selectedItems);
      }
    });
  // Add all Bootstrap tooltips
  $('[data-toggle="tooltip"]').tooltip();
  }
);

function getSelectedItems() {
  selectedItems = JSON.parse(sessionStorage.getItem('selectedItems'));
  if (selectedItems == null){
    selectedItems = [];
  }
  return selectedItems;
}

function saveSelectedItems(selectedItems) {
  sessionStorage.setItem('selectedItems', JSON.stringify(selectedItems));
}