$('.collage-slideshow').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 0,
  pauseOnHover: false,
  pauseOnFocus: false,
  speed: 30000,
  cssEase: 'linear',
});