let width = $("#collage-container").width();
let height = window.innerHeight;
let anchorTypes = ['.topLeft', '.topRight', '.bottomLeft', '.bottomRight', '.deleteButton'];
let currentDate = Math.round(+new Date()/1000);
let pdfGenerator = new jsPDF();
let selectedItems = [];
let savedCollage = {};
let savedImages = [];
let savedImageFilters = {};
const promises = [];

let stage = new Konva.Stage({
  container: 'collage-container',
  width: width,
  height: height
});

selectedItems = getSelectedItems();
selectedItems.forEach(function(value, index) {
  promises.push(
    $.getJSON(value,
      function(data, err) {
        if (err !== 'success') {
          console.log('Something went wrong: ' + err);
        } else {
          let image_url = data['sequences'][0]['canvases'][0]['images'][0]['resource']['@id'];
          let url_array = image_url.split("/");
          let image_id = 'image-'+url_array[5];
          savedImages.push({name:image_id, value:image_url});
        }
    }));
});

Promise.all(promises).then(function() {
  savedCollage = getCollage();
  if (!$.isEmptyObject(savedCollage)) {
    stage.destroyChildren();
    layer = Konva.Node.create(savedCollage);
    stage.add(layer);
    layer.getChildren(function(node){
      for (const anchorType of anchorTypes) {
        let foundAnchors = node.find(anchorType);
        foundAnchors.remove();
      }
      let childImage = node.findOne('Image');
      if (childImage) {
        let childID = childImage.id();
        let groupID = childID.replace("image-", "group-");
        let imageID = childID.substr(0, childID.lastIndexOf('-'));
        let imageObj = new Image();
        let found = savedImages.filter(function (item) {
          return item.name === imageID;
        });
        if (0 in found) {
          imageObj.src = found[0].value;
          imageObj.crossOrigin = "Anonymous";
          imageObj.onload = function () {
            stage.find('#' + childID)[0].image(imageObj);
            // Canvas should no longer be draggable
            node.draggable(false);
            // Add the blur filter
            node.filters([Konva.Filters.Blur]);
            savedImageFilters = getImageFilters();

            if (groupID in savedImageFilters) {
              if ('blurRadius' in savedImageFilters[groupID]) {
                let blurRadius = savedImageFilters[groupID]['blurRadius'];
                if (blurRadius) {
                  node.blurRadius(blurRadius);
                }
              }
            }
            //add blend mode
            node.attrs.globalCompositeOperation = childImage.attrs.globalCompositeOperation;

            node.cache();
            stage.draw();
          };
        }
      }
      layer.draw();
    });
  }
});

function getSelectedItems() {
  selectedItems = JSON.parse(sessionStorage.getItem('selectedItems'));
  if (selectedItems == null){
    selectedItems = [];
  }
  return selectedItems;
}

function getCollage() {
  savedCollage = JSON.parse(sessionStorage.getItem('savedCollage'));
  if (savedCollage == null){
    savedCollage = [];
  }
  return savedCollage;
}

function getImageFilters() {
  savedImageFilters = JSON.parse(sessionStorage.getItem('savedImageFilters'));
  if (savedImageFilters == null){
    savedImageFilters = [];
  }
  return savedImageFilters;
}

$( "#share-form" ).submit(function( event ) {
  event.preventDefault();
  let dataURL = stage.toDataURL({
    mimeType: 'image/png',
  });
  pdfGenerator.addImage(dataURL, 'PNG', 0, 0);
  pdfGenerator.save('collage-'+currentDate+'.pdf');
});